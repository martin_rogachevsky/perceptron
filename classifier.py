import numpy as np

class Classifier:
    COEF = 1

    def __init__(self, classesCount, learnData):
        self.w = [np.matrix([0] * 3).transpose()] * classesCount
        self.classesCount = classesCount
        self.learn(learnData)

    def learn(self, data):
        errors = True
        while errors:
            errors = False
            for vector, c in data:
                vector = np.matrix(vector + [1])
                result = self.calculateFuncs(vector)
                if self.needCorrection(c, result):
                    errors = True
                    punishment = self.COEF * vector.transpose()
                    for i in range(len(self.w)):
                        correction = punishment if i == c else ((-1)*punishment if result[i] >= result[c] else 0)
                        self.w[i] = self.w[i] + correction

    def classify(self, vector):
        vector = np.matrix(vector + [1])
        result = list(self.calculateFuncs(vector))
        val = max(result)
        return result.index(val)

    def calculateFuncs(self, vector):
        result = []
        for weight in self.w:
            result.append(int(vector * weight))
        return result

    def needCorrection(self, c, result):
        val = result[c]
        others = (x for i, x in enumerate(result) if i != c)
        return not all((x < val for x in others))