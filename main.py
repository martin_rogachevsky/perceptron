from random import randrange
from collections import defaultdict
from classifier import Classifier
from plotBuilder import drawResults
import os

def generateVects(count):
    result = []
    for _ in range(count):
        x = randrange(-100, 100, 5)
        y = randrange(-100, 100, 5)
        result.append([x, y])
    return result

def printW(weights):
    print('Weights:')
    for function_weights in weights:
        print(*(map(int, function_weights)), sep=', ')

def readLearingData(lerningDataFilePath):
    result = []
    with open(lerningDataFilePath, 'r') as dataFile:
        classesCount = int(dataFile.readline())
        for line in dataFile.readlines():
            data = line.split()
            vector = list(map(int, data[:2]))
            result.append((vector, int(data[2])))
    return (classesCount, result)

def main():
    data = readLearingData(os.path.dirname(os.path.abspath(__file__))+'\\data.txt')
    classifier = Classifier(data[0], data[1])
    result = defaultdict(list)
    for v in data[1]:
        vector = v[0]
        result[classifier.classify(vector)].append(vector)
    randomVects = generateVects(100)
    for randomVects in randomVects:
        result[classifier.classify(randomVects)].append(randomVects)
    drawResults(result)
    printW(classifier.w)

main()
